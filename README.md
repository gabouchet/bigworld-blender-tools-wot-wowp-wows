# BigWorld - Blender Tools (WoT, WoWP, WoWS) #

Набор плагинов для 3D-редактора Blender, позволяющий просматривать модели из ресурсов игр World of Tanks, World of Warplanes и World of Warships.

Изначально разрабатывался как замена устаревшей программе [WoT Tank Viewer](http://www.koreanrandom.com/forum/topic/1495-wot-tank-viewer-version-1017/).

### Перечень плагинов в архиве: ###

**World of Tanks:**

1. tank_viewer - плагин для просмотра моделей танков из World of Tanks
1. wot_maps_viewer - плагин для просмотра карт  из World of Tanks

**World of Warplanes**

1. airplane_viewer - плагин для просмотра моделей самолётов из World of Warplanes

**World of Warships**

1. warship_viewer - плагин для просмотра моделей кораблей из World of Warships

**Другие инструменты:**

1. io_bigworld_model - плагин импорта/экспорта моделей Bigworld.

### Установка ###

Требования: [Blender](http://www.blender.org/) 2.93 или новее

1. [Скачайте плагин в разделе загрузок](https://bitbucket.org/SkepticalFox/bigworld-blender-tools-wot-wowp-wows/downloads/).
1. Поместите папку с нужным плагином из скачанного архива в папку `C:\Program Files\Blender Foundation\Blender\*<version>*\scripts\addons_contrib`
1. Включите плагин в настройках Blender (File->User Preferences...)
1. Нажмите **Save User Settings** для того, чтобы при следующем открытии Blender этот плагин был включен.

### Поддержка и разработка ###

Поддержка и разработка плагина осуществляется в [официальной теме плагина](http://www.koreanrandom.com/forum/topic/28240-blender-tank-viewer/) на форумах Korean Random.

### Разработчики ###
[SkepticalFox](http://www.koreanrandom.com/forum/user/16296-skepticalfox/)

* * *

# BigWorld - Blender Tools (WoT, WoWP, WoWS) #

This is a package of plugins for Blender, which allows you view 3D models from World of Tanks, World of Warplanes and World of Warships.

Originally developed as open-source replacement for [WoT Tank Viewer](http://www.koreanrandom.com/forum/topic/1495-wot-tank-viewer-version-1017/) which development is discontinued now.

### List of Plugins: ###

**World of Tanks:**

1. tank_viewer -  tank model viewer for World of Tanks
1. wot_maps_viewer - map viewer for  World of Tanks

**World of Warplanes**

1. airplane_viewer - plane model viewer World of Warplanes

**World of Warships**

1. warship_viewer - ship viewer for World of Warships

**Другие инструменты:**

1. io_bigworld_model - Import-Export plugin for Bigworld's 3D models.

### Installation ###

Requirements: [Blender](http://www.blender.org/) 2.93 or newer

1. [Download plugin](https://bitbucket.org/SkepticalFox/bigworld-blender-tools-wot-wowp-wows/downloads).
1. Place necessary folder(s) from downloaded archive to `C:\Program Files\Blender Foundation\Blender\*<version>*\scripts\addons_contrib`
1. Enable plugin in Blender (File->User Preferences...)
1. Click **Save User Settings** to run this plugin automatically with Blender.

### Development and Support ###

[Official topic](http://www.koreanrandom.com/forum/topic/28240-blender-tank-viewer/) on Korean Random's forums.

On this forums most messages written in Russian, and you can use [Google Translate](https://translate.google.com/), if you want understand the essence.
You can write in english in any topics on our forum. You can ask your questions in English, and get answers to them in English as well.

Also you can change the interface language to English for your convenience:

![Screenshot: How to change interface Language on forums](http://www.modxvm.com/assets/additional/kr.cm_lang_menu.png)

### Developers ###
[SkepticalFox](http://www.koreanrandom.com/forum/user/16296-skepticalfox/)
