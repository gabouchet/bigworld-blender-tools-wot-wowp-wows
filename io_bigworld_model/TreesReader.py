"""
Thanks Coffee_ for providing usefull informations about ctree format
"""



__all__ = ('g_TreesReader', )



from struct import unpack

from mathutils import Vector



def unp(arg, value):
    return unpack(arg, value)[0];



SINGLE_FORMAT = 'normal'
TRIPLE_FORMAT = 'triple'



class TreesReader:
    """ Reads .ctree files """

    def read(self, f):
        f.seek(36)

        # Prepare for objects
        objects = []

        # Each object has different properties
        names = ['stock', 'branches', 'leaves', 'billboard']
        vertices_sizes = [52,52,88,68]
        formats = [SINGLE_FORMAT, SINGLE_FORMAT, TRIPLE_FORMAT, TRIPLE_FORMAT]

        # Read each object
        for _m in range(4):

            # First is vertice count
            vertices_count = unp('<I', f.read(4))
            vertices = []

            # Then read each vertex
            for i in range(vertices_count):
                data = f.read(vertices_sizes[_m])

                vert = Vertex()
                vert.position = Vector(unpack('<3f', data[0:12]))
                vert.normal = Vector(unpack('<3f', data[12:24]))
                vert.uv = Vector(unpack('<2f', data[24:32]))
                vert.uv.y = -vert.uv.y

                if _m == 3:
                    vert.uv = Vector(unpack('<2f', data[9*4:11*4]))
                    vert.uv.y = -vert.uv.y

                elif _m == 2:
                    vert.geomInfo = Vector(unpack('<2f', data[4*12:4*14]))
                    vert.vn = int(unp('<f', data[4*15:4*16]))

                vertices.append(vert)

            # Now, read number of lods for this object
            indices_lods = unp('<I', f.read(4))

            # Load indices for each lod
            lods = []
            for lod in range(indices_lods):
                indices_count = unp('<I', f.read(4))
                indices = unpack('<%dI' % indices_count, f.read(4*indices_count))
                lods.append(indices)

            # Now load textures
            texture = f.read(unp('<I', f.read(4))).decode('utf-8')
            texture2 = f.read(unp('<I', f.read(4))).decode('utf-8')

            # Save to result, if there is anything to save
            if vertices:
                objects.append(TreeObject(names[_m], vertices, lods, [texture, texture2], formats[_m]))

        return Tree(objects)



class Vertex:
    position = None
    normal = None
    uv = None

    geomInfo = None
    index = None



class Tree:    
    objects = None

    def __init__(self, objects):
        self.objects = objects



class TreeObject:
    name = None
    vertices = None
    indices = None
    indicesFormat = None
    textures = None
    lods = 0

    def __init__(self, name, vertices, indices, textures, indicesFormat):
        self.name = name
        self.indices = indices
        self.vertices = vertices
        self.textures = textures
        self.lods = len(indices)
        self.indicesFormat = indicesFormat



g_TreesReader = TreesReader()
