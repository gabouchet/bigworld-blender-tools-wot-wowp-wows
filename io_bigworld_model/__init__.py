''' SkepticalFox 2015-2022 '''



#####################################################################
# Addon - header

bl_info = {
    "name": "BigWorld Model (.primitives)",
    "author": "SkepticalFox",
    "version": (0, 0, 17),
    "blender": (2, 93, 0),
    "location": "File > Import-Export",
    "description": "BigWorld Model Import/Export plugin",
    "warning": "Test version",
    "wiki_url": "http://www.koreanrandom.com/forum/topic/28240-/",
    "category": "Import-Export",
}



#####################################################################
# imports

import os

from .common import *
from .import_bw_primitives import BigWorldModelLoader

import bpy

from mathutils import Vector
from bpy_extras.io_utils import ImportHelper, ExportHelper



#####################################################################
# unregister

def unregister():
    bpy.types.TOPBAR_MT_file_import.remove(menu_func_import)
    bpy.types.TOPBAR_MT_file_import.remove(menu_func_import_ctree)
    bpy.types.TOPBAR_MT_file_export.remove(menu_func_export)
    bpy.utils.unregister_class(BigWorld_Material_Panel)
    bpy.utils.unregister_class(Import_From_CtreeFile)
    bpy.utils.unregister_class(Import_From_ModelFile)
    bpy.utils.unregister_class(Export_ModelFile)



#####################################################################
# register

def register():
    bpy.types.TOPBAR_MT_file_import.append(menu_func_import)
    bpy.types.TOPBAR_MT_file_import.append(menu_func_import_ctree)
    bpy.types.TOPBAR_MT_file_export.append(menu_func_export)

    bpy.types.Material.BigWorld_Shader_Path = bpy.props.StringProperty(
        name = 'fx',
        default = 'shaders/std_effects/lightonly.fx',
        description = 'DirectX Shader'
    )

    for val in (
                ('normalMap', 'The normal map for the material'),
                ('specularMap', 'The specular map for the material'),
                ('diffuseMap', 'The diffuse map for the material'),
                ('metallicDetailMap', ''),
                ('metallicGlossMap', ''),
                ('excludeMaskAndAOMap', ''),
                ('g_detailMap', ''),
                ('diffuseMap2', 'The diffuse map2 for the materials'),
                ('crashTileMap', ''),
                ('g_albedoConversions', ''),
                ('g_glossConversions', ''),
                ('g_metallicConversions', ''),
                ('g_detailUVTiling', ''),
                ('g_albedoCorrection', ''),
                ('g_detailRejectTiling', ''),
                ('g_detailInfluences', ''),
                ('g_crashUVTiling', ''),
                ('g_defaultPBSConversionParams', '(true/false)'),
                ('g_useDetailMetallic', '(true/false)'),
                ('g_useNormalPackDXT1', '(true/false)'),
                ('alphaTestEnable', 'Whether an alpha test should be performed (true/false)'),
                ('doubleSided', 'Whether the material is draw on both sides (true/false)'),
                ('alphaReference', 'The alpha value cut-off value (0..255)'),
                ('g_detailPowerGloss', ''),
                ('g_detailPowerAlbedo', ''),
                ('g_maskBias', ''),
                ('g_detailPower', ''),
                ('groupOrigin', '')):
        exec('bpy.types.Material.BigWorld_%s=bpy.props.StringProperty(name=val[0],description=val[1])' % val[0])

    bpy.utils.register_class(BigWorld_Material_Panel)
    bpy.utils.register_class(Import_From_CtreeFile)
    bpy.utils.register_class(Import_From_ModelFile)
    bpy.utils.register_class(Export_ModelFile)



#####################################################################
# menu_func

def menu_func_import_ctree(self, context):
    self.layout.operator('import_model.ctree_model', text = 'BigWorld (.ctree)')



def menu_func_import(self, context):
    self.layout.operator('import_model.bwmodel', text = 'BigWorld (.model)')



def menu_func_export(self, context):
    self.layout.operator('export_model.bwmodel', text='BigWorld (.model)')



#####################################################################
# BigWorld Material Panel

class BigWorld_Material_Panel(bpy.types.Panel):
    bl_label = 'BigWorld Material'
    bl_idname = 'MATERIAL_PT_bigworld_material'
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_options = {'DEFAULT_CLOSED'}
    bl_context = 'material'

    def draw(self, context):
        layout = self.layout

        mat = context.material

        if mat:
            layout.prop(mat, 'BigWorld_Shader_Path')
            # layout.prop(mat, 'BigWorld_mfm_Path')

            layout.separator()

            layout.label('Texture:')
            layout.prop(mat, 'BigWorld_diffuseMap')
            layout.prop(mat, 'BigWorld_normalMap')
            layout.prop(mat, 'BigWorld_specularMap')
            layout.prop(mat, 'BigWorld_metallicGlossMap')
            layout.prop(mat, 'BigWorld_metallicDetailMap')
            layout.prop(mat, 'BigWorld_excludeMaskAndAOMap')
            layout.prop(mat, 'BigWorld_g_detailMap')
            layout.prop(mat, 'BigWorld_diffuseMap2')
            layout.prop(mat, 'BigWorld_crashTileMap')

            layout.separator()

            layout.label('Vector4:')
            layout.prop(mat, 'BigWorld_g_albedoConversions')
            layout.prop(mat, 'BigWorld_g_glossConversions')
            layout.prop(mat, 'BigWorld_g_metallicConversions')
            layout.prop(mat, 'BigWorld_g_detailUVTiling')
            layout.prop(mat, 'BigWorld_g_albedoCorrection')
            layout.prop(mat, 'BigWorld_g_detailRejectTiling')
            layout.prop(mat, 'BigWorld_g_detailInfluences')
            layout.prop(mat, 'BigWorld_g_crashUVTiling')
            # layout.prop(mat, 'BigWorld_uTransform')
            # layout.prop(mat, 'BigWorld_vTransform')

            layout.separator()

            layout.label('Bool:')
            layout.prop(mat, 'BigWorld_g_defaultPBSConversionParams')
            layout.prop(mat, 'BigWorld_g_useDetailMetallic')
            layout.prop(mat, 'BigWorld_g_useNormalPackDXT1')
            layout.prop(mat, 'BigWorld_alphaTestEnable')
            layout.prop(mat, 'BigWorld_doubleSided')
            # layout.prop(mat, 'BigWorld_dynamicObject')

            layout.separator()

            layout.label('Int:')
            layout.prop(mat, 'BigWorld_alphaReference')
            # layout.prop(mat, 'BigWorld_texAddressMode')
            # layout.prop(mat, 'BigWorld_textureOperation')

            layout.separator()

            layout.label('Float:')
            layout.prop(mat, 'BigWorld_g_detailPowerGloss')
            layout.prop(mat, 'BigWorld_g_detailPowerAlbedo')
            layout.prop(mat, 'BigWorld_g_maskBias')
            layout.prop(mat, 'BigWorld_g_detailPower')
            # layout.prop(mat, 'BigWorld_selfIllumination')

            layout.prop(mat, 'BigWorld_groupOrigin')



#####################################################################
# Import_From_CtreeFile

class Import_From_CtreeFile(bpy.types.Operator, ImportHelper):
    bl_idname = 'import_model.ctree_model'
    bl_label = 'Import Ctree Model'
    bl_description = 'Import BigWorld Ctree Model'
    bl_options = {'UNDO'}

    filename_ext = '.ctree'
    filter_glob: bpy.props.StringProperty(
        default = '*.ctree',
        options = {'HIDDEN'}
    )

    def execute(self, context):
        from . import loadctree
        loadctree.load(self.filepath)
        return {'FINISHED'}



#####################################################################
# Import_From_ModelFile

class Import_From_ModelFile(bpy.types.Operator, ImportHelper):
    bl_idname = 'import_model.bwmodel'
    bl_label = 'Import Model'
    bl_description = 'Import BigWorld Model'
    bl_options = {'UNDO'}

    filename_ext = '.model;.visual*;.primitives*'
    filter_glob: bpy.props.StringProperty(
        default = '*.temp_model;*.model;*.visual*;*.primitives*',
        options = {'HIDDEN'}
    )

    import_empty: bpy.props.BoolProperty(
        name = 'Import Empty',
        description = '',
        default = True
    )

    def execute(self, context):
        shr_PrintSplitter()
        shr_PrintInfo('import `%s`' % self.filepath)
        try:
            bw_model = BigWorldModelLoader()
            bw_model.load_from_file(self.filepath, self.import_empty)
        except:
            self.report({'ERROR'}, 'Error in import `%s`!' % self.filepath)
            import traceback
            traceback.print_exc()
        return {'FINISHED'}

    def draw(self, context):
        layout = self.layout

        layout.prop(self, 'import_empty')



#####################################################################
# get_nodes_by_empty

def get_nodes_by_empty(obj, export_info, is_root=True):
    if is_root:
        node_name = 'Scene Root'
    else:
        node_name = os.path.splitext(obj.name)[0]
    export_info[node_name] = {
        'loc': obj.location.xzy.to_tuple(),
        'scale': obj.scale.xzy.to_tuple(),
        'children': {}
    }
    obj_models = []
    for child in obj.children:
        if (child.data is None) and isinstance(child, bpy.types.Object):
            get_nodes_by_empty(child, export_info[node_name]['children'], False)
        elif isinstance(child.data, bpy.types.Mesh):
            obj_models.append(child)
    return obj_models



#####################################################################
# Export_ModelFile

class Export_ModelFile(bpy.types.Operator, ExportHelper):
    bl_idname = 'export_model.bwmodel'
    bl_label = 'Export Model'
    bl_description = 'Export BigWorld Model'

    filename_ext = '.temp_model'
    filter_glob: bpy.props.StringProperty(
        default = '*.temp_model',
        options = {'HIDDEN'}
    )

    is_processed: bpy.props.BoolProperty(
        name = 'Processed',
        description = 'Only for WoT 0.9.12+',
        default = False
    )

    visual_type: bpy.props.EnumProperty(
        name = 'Visual type',
        description = '',
        items = (
            ('0', 'Static', 'No bones'),
            ('1', 'Animated, skinned', 'Bones, rigged')
        )
    )

    @classmethod
    def poll(self, context):
        sel_obj = context.selected_objects
        if len(sel_obj) == 1:
            return (sel_obj[0].data is None) and isinstance(sel_obj[0], bpy.types.Object) and sel_obj[0].children
        return False

    def get_export_object(self, obj_models):
        bpy.ops.object.select_all(action='DESELECT')
        tmp_ctx = bpy.context.copy()
        obs = []
        for obj_model in obj_models:
            new_obj = obj_model.copy()
            new_obj.data = new_obj.data.copy()
            obs.append(new_obj)
        print(obs)
        tmp_ctx['selected_objects'] = tmp_ctx['selected_editable_objects'] = obs
        tmp_ctx['object'] = tmp_ctx['active_object'] = tmp_ctx['selected_objects'][0]
        if len(tmp_ctx['selected_objects']) > 1:
            bpy.ops.object.join(tmp_ctx)
        return tmp_ctx['selected_objects'][0]

    def execute(self, context):
        obj = context.selected_objects[0]
        export_info = {
            'nodes' : {}
        }
        obj_models = get_nodes_by_empty(obj, export_info['nodes'])

        if len(obj_models):
            bb_min = Vector((10000.0, 10000.0, 10000.0))
            bb_max = Vector((-10000.0, -10000.0, -10000.0))

            export_info['exporter_version'] = '%s.%s.%s' % bl_info['version']

            if self.visual_type == '0':
                export_model = self.get_export_object(obj_models)

                export_model.data.transform(export_model.matrix_world)

                bb_min.x = min(export_model.bound_box[0][0], bb_min.x)
                bb_min.z = min(export_model.bound_box[0][1], bb_min.z)
                bb_min.y = min(export_model.bound_box[0][2], bb_min.y)

                bb_max.x = max(export_model.bound_box[6][0], bb_max.x)
                bb_max.z = max(export_model.bound_box[6][1], bb_max.z)
                bb_max.y = max(export_model.bound_box[6][2], bb_max.y)

                export_info['bb_min'] = bb_min.to_tuple()
                export_info['bb_max'] = bb_max.to_tuple()

                if self.is_processed:
                    from .export_bw_primitives_processed import BigWorldModelExporter
                else:
                    from .export_bw_primitives import BigWorldModelExporter

                bw_exporter = BigWorldModelExporter()
                bw_exporter.export(export_model, self.filepath, export_info)

                bpy.data.objects.remove(export_model)

            elif self.visual_type == '1':
                for obj_model in obj_models:
                    if not obj_model.data.uv_layers:
                        self.report({'ERROR'}, 'mesh.uv_layers is None')
                        shr_PrintError('mesh.uv_layers is None')
                        return {'FINISHED'}

                    if not len(obj_model.data.materials):
                        # TODO:
                        # identifier = empty
                        # mfm = materials/template_mfms/lightonly.mfm
                        self.report({'ERROR'}, 'mesh.materials is None')
                        shr_PrintError('mesh.materials is None')
                        return {'FINISHED'}

                    bb_min.x = min(obj_model.location.x + obj_model.bound_box[0][0], bb_min.x)
                    bb_min.z = min(obj_model.location.y + obj_model.bound_box[0][1], bb_min.z)
                    bb_min.y = min(obj_model.location.z + obj_model.bound_box[0][2], bb_min.y)

                    bb_max.x = max(obj_model.location.x + obj_model.bound_box[6][0], bb_max.x)
                    bb_max.z = max(obj_model.location.y + obj_model.bound_box[6][1], bb_max.z)
                    bb_max.y = max(obj_model.location.z + obj_model.bound_box[6][2], bb_max.y)

                if self.is_processed:
                    from .export_bw_primitives_skinned_processed import BigWorldModelExporter
                else:
                    from .export_bw_primitives_skinned import BigWorldModelExporter

                export_info['bb_min'] = bb_min.to_tuple()
                export_info['bb_max'] = bb_max.to_tuple()

                bw_exporter = BigWorldModelExporter()
                bw_exporter.export(obj_models, self.filepath, export_info)
        return {'FINISHED'}

    def draw(self, context):
        layout = self.layout

        layout.prop(self, 'visual_type')

        layout.separator()

        layout.prop(self, 'is_processed')
