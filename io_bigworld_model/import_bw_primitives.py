''' SkepticalFox 2015-2021 '''



#####################################################################
# imports

import os

from .common import *

from .loaddatamesh import LoadDataMesh
from .common.consts import visual_property_dict

import bpy

from bpy_extras.io_utils import unpack_list
from mathutils import Vector



#####################################################################
# functions

def get_Empty_by_nodes(elem, empty_obj = None):
    if (elem.find('identifier') is None) or (elem.find('transform') is None):
        return None

    identifier = elem.find('identifier').text.strip()
    row0 = shr_AsVector(elem.find('transform').find('row0').text)
    row1 = shr_AsVector(elem.find('transform').find('row1').text)
    row2 = shr_AsVector(elem.find('transform').find('row2').text)
    row3 = shr_AsVector(elem.find('transform').find('row3').text)
    scale = Vector( (row0.x, row2.z, row1.y) )
    location = row3.xzy

    ob = bpy.data.objects.new(identifier, None)

    ob.scale = scale
    ob.location = location

    if empty_obj is not None:
        ob.parent = empty_obj

    bpy.context.view_layer.active_layer_collection.collection.objects.link(ob)

    for node in elem.findall('node'):
        get_Empty_by_nodes(node, ob)

    return ob



#####################################################################
# BigWorldModelLoader

class BigWorldModelLoader:

    root_empty_ob = None

    def load_from_file(self, model_filepath, import_empty = False):

        model_dir = os.path.dirname(model_filepath)
        model_filename = os.path.basename(model_filepath)

        visual_filename = '%s.visual_processed' % os.path.splitext(model_filename)[0]
        visual_filename_old = '%s.visual' % os.path.splitext(model_filename)[0]
        primitives_filename = '%s.primitives_processed' % os.path.splitext(model_filename)[0]
        primitives_filename_old = '%s.primitives' % os.path.splitext(model_filename)[0]

        visual_filepath = ''
        primitives_filepath = ''
        if os.path.exists(os.path.join(model_dir, visual_filename)):
            visual_filepath = os.path.join(model_dir, visual_filename)
            primitives_filepath = os.path.join(model_dir, primitives_filename)

        elif os.path.exists(os.path.join(model_dir, visual_filename_old)):
            visual_filepath = os.path.join(model_dir, visual_filename_old)
            primitives_filepath = os.path.join(model_dir, primitives_filename_old)

        if visual_filepath and primitives_filepath:
            with open(visual_filepath, 'rb') as f:
                visual = g_XmlUnpacker.read(f)

            if visual.find('renderSet') is not None:
                for renderSet in visual.findall('renderSet'):
                    vres_name = renderSet.find('geometry/vertices').text.strip()
                    pres_name = renderSet.find('geometry/primitive').text.strip()
                    mesh_name = os.path.splitext(vres_name)[0]
                    bmesh = bpy.data.meshes.new(mesh_name)

                    uv2_name = ''
                    if renderSet.find('geometry/stream') is not None:
                        stream_res_name = renderSet.find('geometry/stream').text.strip()
                        if 'uv2' in stream_res_name:
                            uv2_name = stream_res_name

                    print(primitives_filepath, vres_name, pres_name)
                    dataMesh = LoadDataMesh(primitives_filepath, vres_name, pres_name, uv2_name)

                    bmesh.vertices.add(len(dataMesh.vertices))
                    bmesh.vertices.foreach_set('co', unpack_list(dataMesh.vertices))

                    # TODO:
                    # bmesh.vertices.foreach_set('normal', unpack_list(dataMesh.normal_list))

                    nbr_faces = len(dataMesh.indices)
                    bmesh.polygons.add(nbr_faces)

                    bmesh.polygons.foreach_set('loop_start', range(0, nbr_faces*3, 3))
                    bmesh.polygons.foreach_set('loop_total', (3,)*nbr_faces)

                    bmesh.loops.add(nbr_faces*3)
                    bmesh.loops.foreach_set('vertex_index', unpack_list(dataMesh.indices))

                    nbr_faces = len(bmesh.polygons)
                    bmesh.polygons.foreach_set('use_smooth', [True]*nbr_faces)

                    uv2_faces = None
                    if uv2_name:
                        if dataMesh.uv2_list:
                            uv2_faces = bmesh.uv_layers.new()
                            uv2_faces.name = 'uv2'
                        else:
                            uv2_name = ''

                    if dataMesh.uv_list:
                        uv_faces = bmesh.uv_layers.new()
                        uv_faces.name = 'uv1'
                        uv_faces.active = True

                        uv_layer = bmesh.uv_layers['uv1'].data[:]
                        uv2_layer = bmesh.uv_layers['uv2'].data[:] if uv2_faces else None

                        for poly in bmesh.polygons:
                            for li in poly.loop_indices:
                                vi = bmesh.loops[li].vertex_index
                                uv_layer[li].uv = dataMesh.uv_list[vi]
                                if uv2_name:
                                    uv2_layer[li].uv = dataMesh.uv2_list[vi]
                    else:
                        print('Warn: uv_faces is None')

                    for primitiveGroup in renderSet.findall('geometry/primitiveGroup'):
                        _identifier = primitiveGroup.find('material/identifier').text.strip()
                        _index = int(primitiveGroup.text)

                        material = bpy.data.materials.new(_identifier)

                        if primitiveGroup.find('material/fx') is not None:
                            material.BigWorld_Shader_Path = primitiveGroup.find('material/fx').text.strip()

                        for property in primitiveGroup.findall('material/property'):
                            try:
                                property_name_xml = property.text.strip()
                                for prop_type, prop_names in visual_property_dict.items():
                                    if property_name_xml in prop_names:
                                        exec('material.BigWorld_%s = property[0].text' % property_name_xml)
                                        break
                            except:pass

                        if primitiveGroup.find('groupOrigin') is not None:
                            material.BigWorld_groupOrigin = primitiveGroup.find('groupOrigin').text.strip()

                        bmesh.materials.append(material)

                        startIndex = dataMesh.PrimitiveGroups[_index]['startIndex']//3
                        endPrimitives = startIndex + dataMesh.PrimitiveGroups[_index]['nPrimitives']

                        for fidx, pl in enumerate(bmesh.polygons):
                            if startIndex <= fidx <= endPrimitives:
                                pl.material_index = _index
                    
                    bmesh.validate()
                    bmesh.update()

                    ob = bpy.data.objects.new(mesh_name, bmesh)

                    if 'true' in renderSet.find('treatAsWorldSpaceObject').text:
                        if dataMesh.bones_info:
                            bone_arr = []
                            for node in renderSet.findall('node'):
                                bone_name = node.text.strip()
                                group = ob.vertex_groups.new( name = bone_name )
                                bone_arr.append({'name': bone_name, 'group': group})

                            for vert_idx, iiiww in enumerate(dataMesh.bones_info):
                                if len(iiiww) == 5:
                                    bone_idx_vals = iiiww[0:3]
                                    bone_wght_vals = iiiww[3:5]

                                    index1 = bone_idx_vals[0]//3
                                    index2 = bone_idx_vals[1]//3
                                    index3 = bone_idx_vals[2]//3

                                    weight1 = bone_wght_vals[0]/255.0
                                    weight2 = bone_wght_vals[1]/255.0
                                    weight3 = 1 - weight1 - weight2

                                    # types:
                                    # 'ADD', 'REPLACE', 'SUBTRACT'
                                    bone_arr[index1]['group'].add([vert_idx], weight1, 'ADD')
                                    bone_arr[index2]['group'].add([vert_idx], weight2, 'ADD')
                                    bone_arr[index3]['group'].add([vert_idx], weight3, 'ADD')

                                elif len(iiiww) == 8:
                                    bone_idx_vals = iiiww[0:3]
                                    bone_wght_vals = iiiww[6:8]

                                    index1 = bone_idx_vals[0]//3
                                    index2 = bone_idx_vals[1]//3
                                    index3 = bone_idx_vals[2]//3

                                    weight1 = bone_wght_vals[0]/255.0
                                    weight2 = bone_wght_vals[1]/255.0
                                    weight3 = 1 - weight1 - weight2

                                    # types:
                                    # 'ADD', 'REPLACE', 'SUBTRACT'
                                    bone_arr[index1]['group'].add([vert_idx], weight3, 'ADD')
                                    bone_arr[index2]['group'].add([vert_idx], weight2, 'ADD')
                                    bone_arr[index3]['group'].add([vert_idx], weight1, 'ADD')

                    ob.scale = Vector((1.0, 1.0, 1.0)) 
                    ob.rotation_euler = Vector((0.0, 0.0, 0.0)) 
                    ob.location = Vector((0.0, 0.0, 0.0)) 

                    if import_empty:
                        if visual.find('node') is not None:
                            if self.root_empty_ob is None:
                                self.root_empty_ob = get_Empty_by_nodes(visual.findall('node')[0])
                            if self.root_empty_ob is not None:
                                ob.parent = self.root_empty_ob

                    bpy.context.view_layer.active_layer_collection.collection.objects.link(ob)
