''' SkepticalFox 2015-2018 '''



import os

import bpy

from mathutils import Vector
from bpy_extras.image_utils import load_image

from .TreesReader import *



def load(filepath):
    with open(filepath, 'rb') as f:
        tree = g_TreesReader.read(f)
    for obj in tree.objects:
        faces = []
        if obj.name == 'stock':
            for triStrip in obj.indices:
                for i in range(len(triStrip)-2):
                    if i % 2 == 0:
                        a = triStrip[i+0]
                        b = triStrip[i+1]
                        c = triStrip[i+2]
                    else:
                        a = triStrip[i+2]
                        b = triStrip[i+1]
                        c = triStrip[i+0]
                    faces.append([a, b, c])
        elif obj.name == 'billboard':
            for triStrip in obj.indices:
                for i in range(0, len(triStrip)-3, 3):
                    faces.append(triStrip[i:i+3])
        elif obj.name == 'branches':
            for triStrip in obj.indices:
                for i in range(len(triStrip)-2):
                    if i % 2 == 0:
                        a = triStrip[i+0]
                        b = triStrip[i+1]
                        c = triStrip[i+2]
                    else:
                        a = triStrip[i+2]
                        b = triStrip[i+1]
                        c = triStrip[i+0]
                    faces.append([a, b, c])
        elif obj.name == 'leaves':
            for triStrip in obj.indices:
                for i in range(0, len(triStrip)-3, 3):
                    faces.append(triStrip[i:i+3])
        bmesh = bpy.data.meshes.new(obj.name)

        verts = [v.position.xzy for v in obj.vertices]

        bmesh.from_pydata(verts, [], faces)

        uv_faces = bmesh.uv_textures.new()
        uv_faces.active = True

        uv_layer = bmesh.uv_layers[uv_faces.name].data[:]

        for poly in bmesh.polygons:
            for li in poly.loop_indices:
                vi = bmesh.loops[li].vertex_index
                uv_layer[li].uv = obj.vertices[vi].uv

        assert(len(obj.textures) == 2)
        diffuseMap, normalMap = obj.textures

        material = bpy.data.materials.new('Material_' + obj.name)
        material.specular_intensity = 0.0
        material.use_transparency = True
        material.alpha = 0.0

        path_ = os.path.dirname(filepath)
        diffuseMap = os.path.join(path_, os.path.basename(diffuseMap))
        normalMap = os.path.join(path_, os.path.basename(normalMap))

        if os.path.exists(diffuseMap):
            texture = bpy.data.textures.new(name='diffuseMap_' + obj.name, type='IMAGE')
            img = load_image(diffuseMap)
            texture.image = img
            mtex = material.texture_slots.add()
            mtex.use_map_alpha = True
            mtex.texture = texture
        if os.path.exists(normalMap):
            texture = bpy.data.textures.new(name='normalMap_' + obj.name, type='IMAGE')
            img = load_image(normalMap)
            texture.image = img
            mtex = material.texture_slots.add()
            mtex.use_map_color_diffuse = False
            mtex.use_map_normal = True
            mtex.normal_factor = 0.095
            mtex.texture = texture

        bmesh.materials.append(material)

        bmesh.show_double_sided = True
        bmesh.validate()
        bmesh.update()
        ob = bpy.data.objects.new(obj.name, bmesh)
        ob.location = Vector((0, 0, 0))
        bpy.context.scene.objects.link(ob)
