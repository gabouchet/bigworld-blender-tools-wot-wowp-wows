# Blender Tank Viewer (World of Tanks) #

Этот плагин для 3D-редактора Blender разрабатывается как замена устаревшей программе [WoT Tank Viewer](http://www.koreanrandom.com/forum/topic/1495-wot-tank-viewer-version-1017/).

Плагин предназначен для просмотра моделей танков из игры World of Tanks.

### Установка ###

Требования: [Blender](http://www.blender.org/) 2.78 или новее

1. [Скачайте плагин в разделе загрузок](https://bitbucket.org/SkepticalFox/bigworld-blender-tools-wot-wowp-wows/downloads).
1. Поместите папку `tank_viewer` из архива в папку `C:\Program Files\Blender Foundation\Blender\2.76\scripts\addons_contrib`
1. Включите плагин в настройках Blender (File->User Preferences...)
1. Нажмите **Save User Settings** для того, чтобы при следующем открытии Blender этот плагин был включен.

### Поддержка и разработка ###

Поддержка и разработка плагина осуществляется в [официальной теме плагина](http://www.koreanrandom.com/forum/topic/28240-blender-tank-viewer/) на форумах Korean Random.

### Разработчики ###
[SkepticalFox](http://www.koreanrandom.com/forum/user/16296-skepticalfox/)

* * *

# Blender Tank Viewer (World of Tanks) #

This plugin for Blender developed as open-source replacement for [WoT Tank Viewer](http://www.koreanrandom.com/forum/topic/1495-wot-tank-viewer-version-1017/) which development is discontinued now.

The plugin is intended for viewing 3D models of tanks from the game World of Tanks.

### Installation ###

Requirements: [Blender](http://www.blender.org/) 2.78 or newer

1. [Download plugin](https://bitbucket.org/SkepticalFox/bigworld-blender-tools-wot-wowp-wows/downloads).
1. Place folder `tank_viewer` from downloaded archive to `C:\Program Files\Blender Foundation\Blender\2.76\scripts\addons_contrib`
1. Enable plugin in Blender (File->User Preferences...)
1. Click **Save User Settings** to run this plugin automatically with Blender.

### Development and Support ###

[Official topic](http://www.koreanrandom.com/forum/topic/28240-blender-tank-viewer/) on Korean Random forums.

On this forums most messages written in Russian, and you can use [Google Translate](https://translate.google.com/), if you want understand the essence.
You can write in english in any topics on our forum. You can ask your questions in English, and get answers to them in English as well.

Also you can change the interface language to English for your convenience:

![Screenshot: How to change interface Language on forums](http://www.modxvm.com/assets/additional/kr.cm_lang_menu.png)

### Developers ###
[SkepticalFox](http://www.koreanrandom.com/forum/user/16296-skepticalfox/)
