''' SkepticalFox 2015-2018 '''



__all__ = ('ResourceManager',)



from .common import *
import os

from zipfile import ZipFile
from bpy.app import tempdir



pkg_filename_list = (
    'vehicles_level_01_hd.pkg', 'vehicles_level_01.pkg',
    'vehicles_level_02_hd.pkg', 'vehicles_level_02.pkg',
    'vehicles_level_03_hd.pkg', 'vehicles_level_03.pkg',
    'vehicles_level_04_hd.pkg', 'vehicles_level_04.pkg',

    'vehicles_level_05_hd.pkg', 'vehicles_level_05.pkg',
    'vehicles_level_05_hd-part1.pkg', 'vehicles_level_05_hd-part2.pkg',
    'vehicles_level_05-part1.pkg', 'vehicles_level_05-part2.pkg',

    'vehicles_level_06_hd.pkg', 'vehicles_level_06.pkg',
    'vehicles_level_06_hd-part1.pkg', 'vehicles_level_06_hd-part2.pkg',
    'vehicles_level_06-part1.pkg', 'vehicles_level_06-part2.pkg',

    'vehicles_level_07_hd.pkg', 'vehicles_level_07.pkg',
    'vehicles_level_07_hd-part1.pkg', 'vehicles_level_07_hd-part2.pkg',
    'vehicles_level_07-part1.pkg', 'vehicles_level_07-part2.pkg',

    'vehicles_level_08_hd.pkg', 'vehicles_level_08.pkg',
    'vehicles_level_08_hd-part1.pkg', 'vehicles_level_08_hd-part2.pkg',
    'vehicles_level_08-part1.pkg', 'vehicles_level_08-part2.pkg',

    'vehicles_level_09_hd.pkg', 'vehicles_level_09.pkg',
    'vehicles_level_09_hd-part1.pkg', 'vehicles_level_09_hd-part2.pkg',
    'vehicles_level_09-part1.pkg', 'vehicles_level_09-part2.pkg',

    'vehicles_level_10_hd.pkg', 'vehicles_level_10.pkg',
    'vehicles_level_10_hd-part1.pkg', 'vehicles_level_10_hd-part2.pkg',
    'vehicles_level_10-part1.pkg', 'vehicles_level_10-part2.pkg',

    'shared_content_hd.pkg', 'shared_content.pkg',
    'shared_content_hd-part1.pkg', 'shared_content_hd-part2.pkg',
    'shared_content-part1.pkg', 'shared_content-part2.pkg',

    'shared_content_sandbox_hd.pkg', 'shared_content_sandbox.pkg',
    'shared_content_sandbox_hd-part1.pkg', 'shared_content_sandbox_hd-part2.pkg',
    'shared_content_sandbox-part1.pkg', 'shared_content_sandbox-part2.pkg',

    'hangar_v2_hd.pkg', 'hangar_v2.pkg',
    'hangar_premium_v2_hd.pkg', 'hangar_premium_v2.pkg'
)



class ResourceManager:
    # TODO: res_path: str
    # TODO: scripts_pkg: ZipFile
    # TODO: pkg_zipfile_list: List[ZipFile]
    # TODO: pkg_filepath_dict: Dict[str, tuple]

    def __init__(self, res_path: str):
        self.res_path = res_path
        self.scripts_pkg = None
        self.pkg_zipfile_list = []
        self.pkg_filepath_dict = {}

        scripts_pkg_path = os.path.join(res_path, 'packages', 'scripts.pkg')
        if os.path.isfile(scripts_pkg_path):
            self.scripts_pkg = ZipFile(scripts_pkg_path, 'r')

        pkg_idx = 0
        for pkg in pkg_filename_list:
            pkg_abs_path = os.path.join(res_path, 'packages', pkg)
            if os.path.isfile(pkg_abs_path):
                zfile = ZipFile(pkg_abs_path, 'r')
                self.pkg_zipfile_list.append(zfile)
                for _file in zfile.infolist():
                    self.pkg_filepath_dict[_file.filename.lower()] = (pkg_idx, _file.filename)
                pkg_idx += 1

    def __del__(self):
        self.res_path = ''
        self.pkg_filepath_dict.clear()
        self.pkg_zipfile_list.clear()

    def open_file(self, filename):
        filename = filename.lower()
        if self.pkg_filepath_dict.get(filename):
            pkg_idx, file_path = self.pkg_filepath_dict[filename]
            try:
                from io import BytesIO
                return BytesIO(self.pkg_zipfile_list[pkg_idx].read(file_path))
            except:pass
        return None

    def extract_file(self, file2, file1=''):
        if file1:
            file1 = file1.lower()
            if self.pkg_filepath_dict.get(file1):
                pkg_idx, file_path = self.pkg_filepath_dict[file1]
                try:
                    return self.pkg_zipfile_list[pkg_idx].extract(file_path, tempdir)
                except:pass
        file2 = file2.lower()
        if self.pkg_filepath_dict.get(file2):
            pkg_idx, file_path = self.pkg_filepath_dict[file2]
            try:
                return self.pkg_zipfile_list[pkg_idx].extract(file_path, tempdir)
            except:pass
        return ''

    def open_scripts_xml(self, filename):
        filename = StrToFilePath(filename)
        if self.scripts_pkg is None:
            xml_path = os.path.join(self.res_path, filename)
            if not os.path.isfile(xml_path):
                return
            with open(xml_path, 'rb') as f:
                element = g_XmlUnpacker.read(f)
        else:
            from io import BytesIO
            element = g_XmlUnpacker.read(BytesIO(self.scripts_pkg.read(filename)))
        return element
