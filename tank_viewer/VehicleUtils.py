''' SkepticalFox 2015-2018 '''

from .common import *
from .ResourceManager import ResourceManager

import os
import gettext
import sqlite3
from xml.etree import ElementTree as ET
# TODO: from dataclasses import dataclass
# TODO: from typing import Dict, Any



__all__ = ('WotDataManager', 'g_LevelsL10n', 'g_NationsL10n', 'g_CategoryL10n')



#####################################################################
# TankListPaths

class TankListPaths:
    JAPAN = 'scripts\\item_defs\\vehicles\\japan'
    CHINA = 'scripts\\item_defs\\vehicles\\china'
    CZECH = 'scripts\\item_defs\\vehicles\\czech'
    FRANCE = 'scripts\\item_defs\\vehicles\\france'
    GERMANY = 'scripts\\item_defs\\vehicles\\germany'
    UK = 'scripts\\item_defs\\vehicles\\uk'
    USA = 'scripts\\item_defs\\vehicles\\usa'
    USSR = 'scripts\\item_defs\\vehicles\\ussr'
    SWEDEN = 'scripts\\item_defs\\vehicles\\sweden'
    POLAND = 'scripts\\item_defs\\vehicles\\poland'
    ITALY = 'scripts\\item_defs\\vehicles\\italy'
    INDEX = {
        1: JAPAN, 2: CHINA,
        3: CZECH, 4: FRANCE,
        5: GERMANY, 6: UK,
        7: USA, 8: USSR,
        9: SWEDEN, 10: POLAND,
        11: ITALY
    }



#####################################################################
# MoFiles

class MoFiles:
    JAPAN_MO = 'text\\lc_messages\\japan_vehicles.mo'
    JAPAN_ID = '#japan_vehicles'
    CHINA_MO = 'text\\lc_messages\\china_vehicles.mo'
    CHINA_ID = '#china_vehicles'
    CZECH_MO = 'text\\lc_messages\\czech_vehicles.mo'
    CZECH_ID = '#czech_vehicles'
    FRANCE_MO = 'text\\lc_messages\\france_vehicles.mo'
    FRANCE_ID = '#france_vehicles'
    GERMANY_MO = 'text\\lc_messages\\germany_vehicles.mo'
    GERMANY_ID = '#germany_vehicles'
    UK_MO = 'text\\lc_messages\\gb_vehicles.mo'
    UK_ID = '#gb_vehicles'
    USA_MO = 'text\\lc_messages\\usa_vehicles.mo'
    USA_ID = '#usa_vehicles'
    USSR_MO = 'text\\lc_messages\\ussr_vehicles.mo'
    USSR_ID = '#ussr_vehicles'
    SWEDEN_MO = 'text\\lc_messages\\sweden_vehicles.mo'
    SWEDEN_ID = '#sweden_vehicles'
    POLAND_MO = 'text\\lc_messages\\poland_vehicles.mo'
    POLAND_ID = '#poland_vehicles'
    ITALY_MO = 'text\\lc_messages\\italy_vehicles.mo'
    ITALY_ID = '#italy_vehicles'
    MENU_MO = 'text\\lc_messages\\menu.mo'
    MENU_ID = '#menu'
    ALL = {
        JAPAN_ID: [JAPAN_MO, None],
        CHINA_ID: [CHINA_MO, None],
        CZECH_ID: [CZECH_MO, None],
        FRANCE_ID: [FRANCE_MO, None],
        GERMANY_ID: [GERMANY_MO, None],
        UK_ID: [UK_MO, None],
        USA_ID: [USA_MO, None],
        USSR_ID: [USSR_MO, None],
        SWEDEN_ID: [SWEDEN_MO, None],
        POLAND_ID: [POLAND_MO, None],
        ITALY_ID: [ITALY_MO, None],
        MENU_ID: [MENU_MO, None]
    }

    def load(self, wot_res_path):
        for key, value in self.ALL.items():
            mo_path = os.path.join(wot_res_path, value[0])
            if not os.path.exists(mo_path):
                continue
            self.ALL[key][1] = gettext.GNUTranslations(open(mo_path, 'rb'))

    def _(self, str_):
        try:
            return self.ALL[str_.split(':')[0]][1].gettext(str_.split(':')[1])
        except:
            return str_.split(':')[1]



#####################################################################
# g_MoFiles

g_MoFiles = MoFiles()



#####################################################################
# LevelListL10n

class LevelsL10n:
    _ALL = 'all'
    _FIRST = '1'
    _SECOND = '2'
    _THIRD = '3'
    _FOURTH = '4'
    _FIFTH = '5'
    _SIXTH = '6'
    _SEVENTH = '7'
    _EIGHTH = '8'
    _NINTH = '9'
    _TENTH = '10'
    ALL = {
        _ALL: 0, _FIRST: 1,
        _SECOND: 2, _THIRD: 3,
        _FOURTH: 4, _FIFTH: 5,
        _SIXTH: 6, _SEVENTH: 7,
        _EIGHTH: 8, _NINTH: 9,
        _TENTH: 10
    }
    INDEX = {}

    def load(self):
        for key, value in self.ALL.items():
            self.INDEX[value] = g_MoFiles._('#menu:levels/%s' % key)



#####################################################################
# g_LevelsL10n

g_LevelsL10n = LevelsL10n()



#####################################################################
# NationsL10n

class NationsL10n:
    _ALL = 'all'
    _JAPAN = 'japan'
    _CHINA = 'china'
    _CZEH = 'czech'
    _FRANCE = 'france'
    _GERMANY = 'germany'
    _UK = 'uk'
    _USA = 'usa'
    _USSR = 'ussr'
    _SWEDEN = 'sweden'
    _POLAND = 'poland'
    _ITALY = 'italy'
    ALL = {
        _ALL: 0, _JAPAN: 1,
        _CHINA: 2, _CZEH: 3,
        _FRANCE: 4, _GERMANY: 5,
        _UK: 6, _USA: 7,
        _USSR: 8, _SWEDEN: 9,
        _POLAND: 10, _ITALY: 11
    }
    INDEX = {}

    def load(self):
        for key, value in self.ALL.items():
            self.INDEX[value] = g_MoFiles._('#menu:nations/%s' % key)



#####################################################################
# g_NationsL10ns

g_NationsL10n = NationsL10n()



#####################################################################
# CategoryL10n

class CategoryL10n:
    _ALL = 'all'
    _LIGHT_TANK = 'lightTank'
    _MEDIUM_TANK = 'mediumTank'
    _HEAVY_TANK = 'heavyTank'
    _AT_SPG = 'AT-SPG'
    _SPG = 'SPG'
    ALL = {
        _ALL: 0, _LIGHT_TANK: 1,
        _MEDIUM_TANK: 2, _HEAVY_TANK: 3,
        _AT_SPG: 4, _SPG: 5
    }
    INDEX = {}

    def load(self):
        for key, value in self.ALL.items():
            self.INDEX[value] = g_MoFiles._('#menu:carousel_tank_filter/%s' % key)



#####################################################################
# g_CategoryL10n

g_CategoryL10n = CategoryL10n()



#####################################################################
# helper classes

# TODO: @dataclass(init=False)
class ModelSet:
    # TODO: undamaged_model: str
    # TODO: destroyed_model: str

    def __init__(self, elem):
        self.undamaged_model = StrToFilePath(elem.findtext('undamaged'))
        self.destroyed_model = StrToFilePath(elem.findtext('destroyed'))



# TODO: @dataclass(init=False)
class ChassisSpline:

    # TODO: @dataclass(init=False)
    class SegmentModels:
        # TODO: segmentModelLeft: str
        # TODO: segmentModelRight: str
        # TODO: segment2ModelLeft: str
        # TODO: segment2ModelRight: str

        def __init__(self, elem):
            self.segmentModelLeft = StrToFilePath(elem.findtext('segmentModelLeft'))
            self.segmentModelRight = StrToFilePath(elem.findtext('segmentModelRight'))
            self.segment2ModelLeft = StrToFilePath(elem.findtext('segment2ModelLeft'))
            self.segment2ModelRight = StrToFilePath(elem.findtext('segment2ModelRight'))

        def has_two_segments(self) -> bool:
            return bool(self.segment2ModelLeft and self.segment2ModelRight)

    # TODO: left: str
    # TODO: right: str
    # TODO: right: str
    # TODO: segmentLength: float
    # TODO: segmentOffset: float
    # TODO: segment2Offset: float
    # TODO: sets: Dict[str, SegmentModels]

    def __init__(self, elem):
        self.sets = { 'default' : self.SegmentModels(elem) }

        self.left = StrToFilePath(elem.findtext('left'))
        self.right = StrToFilePath(elem.findtext('right'))

        self.segmentLength = float(elem.findtext('segmentLength'))
        self.segmentOffset = float(elem.findtext('segmentOffset'))

        # TODO: why don't we use it?
        self.segment2Offset = float(elem.findtext('segment2Offset', 0))

        for elem_set in elem.findall('modelSets/*'):
            self.sets[elem_set.tag] = self.SegmentModels(elem_set)



# TODO: @dataclass(init=False)
class ChassisDesc:
    # TODO: collision_model: str
    # TODO: hullPosition: Vector
    # TODO: camouflage_tiling: Vector
    # TODO: splineDesc: ChassisSpline
    # TODO: sets: Dict[str, ModelSet]

    def __init__(self, elem):
        self.collision_model = StrToFilePath(elem.findtext('hitTester/collisionModelClient') or \
                                             elem.findtext('hitTester/collisionModel'))
        self.hullPosition = StrToVector(elem.findtext('hullPosition')).xzy
        self.splineDesc = elem.find('splineDesc') and ChassisSpline(elem.find('splineDesc'))

        self.sets = { 'default' : ModelSet(elem.find('models')) }

        for elem_set in elem.findall('models/sets/*'):
            self.sets[elem_set.tag] = ModelSet(elem_set)



# TODO: @dataclass(init=False)
class HullDesc:
    # TODO: collision_model: str
    # TODO: turretPositions: Vector
    # TODO: camouflage_tiling: Vector
    # TODO: sets: Dict[str, ModelSet]

    def __init__(self, elem, chassis: ChassisDesc):
        self.collision_model = StrToFilePath(elem.findtext('hitTester/collisionModelClient') or \
                                             elem.findtext('hitTester/collisionModel'))
        self.turretPositions = StrToVector(elem.findtext('turretPositions/turret')).xzy + chassis.hullPosition
        # TODO: self.camouflage_tiling = StrToVector(elem.findtext('camouflage/tiling'))
        self.sets = { 'default' : ModelSet(elem.find('models')) }

        for elem_set in elem.findall('models/sets/*'):
            self.sets[elem_set.tag] = ModelSet(elem_set)



# TODO: @dataclass(init=False)
class GunDesc:
    # TODO: userString: str
    # TODO: l10n_name: str
    # TODO: collision_model: str
    # TODO: camouflage_tiling: Vector
    # TODO: sets: Dict[str, ModelSet]

    def __init__(self, elem, turret):
        self.userString = turret.userString.split(':')[0] + ':' + elem.tag
        self.l10n_name = g_MoFiles._(self.userString)

        # TODO: self.camouflage_tiling = StrToVector(elem.findtext('camouflage/tiling'))
        self.collision_model = StrToFilePath(elem.findtext('hitTester/collisionModelClient') or \
                                             elem.findtext('hitTester/collisionModel'))

        self.sets = { 'default' : ModelSet(elem.find('models')) }

        for elem_set in elem.findall('models/sets/*'):
            self.sets[elem_set.tag] = ModelSet(elem_set)



# TODO: @dataclass(init=False)
class TurretDesc:
    # TODO: userString: str
    # TODO: l10n_name: str
    # TODO: collision_model: str
    # TODO: gunPosition: Vector
    # TODO: camouflage_tiling: Vector
    # TODO: sets: Dict[str, ModelSet]
    # TODO: guns: Dict[str, GunDesc]

    def __init__(self, elem, hull: HullDesc):
        # TODO: self.camouflage_tiling = StrToVector(elem.findtext('camouflage/tiling'))
        self.gunPosition = StrToVector(elem.findtext('gunPosition')).xzy + hull.turretPositions
        self.collision_model = StrToFilePath(elem.findtext('hitTester/collisionModelClient') or \
                                             elem.findtext('hitTester/collisionModel'))
        self.userString = elem.findtext('userString').strip()
        self.l10n_name = g_MoFiles._(self.userString)

        self.guns = {}
        for gun_elem in elem.find('guns'):
            gdesc = GunDesc(gun_elem, self)
            self.guns[gdesc.l10n_name] = gdesc

        self.sets = { 'default' : ModelSet(elem.find('models')) }

        for elem_set in elem.findall('models/sets/*'):
            self.sets[elem_set.tag] = ModelSet(elem_set)



# TODO: @dataclass(init=False)
class FullTankDesc:
    # TODO: camouflage_tiling: Vector
    # TODO: camouflage_exclusionMask: Vector
    # TODO: chassis: ChassisDesc
    # TODO: hull: HullDesc
    # TODO: turrets0: Dict[str, TurretDesc]

    def __init__(self, elem):
        # TODO: self.camouflage_tiling = StrToVector(elem.findtext('camouflage/tiling'))
        # TODO: self.camouflage_exclusionMask = StrToVector(elem.findtext('camouflage/exclusionMask'))

        self.chassis = ChassisDesc(elem.find('chassis')[0])
        self.hull = HullDesc(elem.find('hull'), self.chassis)

        self.turrets0 = {}
        for turret_elem in elem.find('turrets0'):
            tdesc = TurretDesc(turret_elem, self.hull)

            # Fix for some SPG-guns:
            tmp = tdesc.l10n_name; i = 0
            while tdesc.l10n_name in self.turrets0:
                i += 1
                tdesc.l10n_name = '{tmp} ({i})'.format(tmp=tmp, i=i)

            self.turrets0[tdesc.l10n_name] = tdesc



# TODO: @dataclass(init=False)
class WotDataManager:
    ''' Main class for working with WoT client '''

    # TODO: wot_res_path: str
    # TODO: wot_version: tuple
    # TODO: res_mgr: ResourceManager
    # TODO: conn: sqlite3.Connection
    # TODO: sqlite3_cursor: sqlite3.Cursor

    @classmethod
    def add_client(cls, wot_dir_path: str):
        wot_res_path = os.path.join(wot_dir_path, 'res')
        wot_version_path = os.path.join(wot_dir_path, 'version.xml')

        if not os.path.isdir(wot_res_path):
            return None

        if not os.path.isfile(wot_version_path):
            return None

        wot_version = cls.get_version(wot_version_path)
        return cls(wot_res_path, wot_version)

    def __init__(self, wot_res_path: str, wot_version: tuple):
        self.wot_res_path = wot_res_path
        self.wot_version = wot_version

        self.conn = sqlite3.connect(':memory:')
        self.sqlite3_cursor = self.conn.cursor()
        self.sqlite3_cursor.execute('CREATE TABLE TankList_table (id INTEGER PRIMARY KEY NOT NULL, tank_nation INTEGER, tank_type_id INTEGER, tank_props INTEGER, tank_name TEXT, tank_name_in_res TEXT, tank_level INTEGER);')

        self.res_mgr = ResourceManager(self.wot_res_path)

        self.loadTranslations()
        self.loadVehicleDictionary()

    def __del__(self):
        print('__del__')
        self.sqlite3_cursor.close()
        self.conn.close()
        del self.res_mgr

    @staticmethod
    def get_version(filename: str) -> tuple:
        ''' Get WoT version from version.xml '''
        tree = ET.parse(filename)
        elem = tree.getroot()
        # Examples:
        # <version> v.0.9.13 #68</version>
        # <version> v.0.9.15.0.1 #44</version>
        # <version> v.0.9.15.1 Common Test #172</version>
        _version = elem.findtext('version').strip()

        PrintSplitter(); PrintInfo('WoT client: {_version}'.format(_version=_version))

        if 'Supertest v.ST ' in _version:
            _version = _version.replace('Supertest v.ST ', 'v.')
        elif ' Common Test' in _version:
            _version = _version.replace(' Common Test', '')
        return tuple(map(int, _version.split('v.')[1].split('#')[0].strip().split('.')))


    def loadTranslations(self):
        g_MoFiles.load(self.wot_res_path)
        g_LevelsL10n.load()
        g_NationsL10n.load()
        g_CategoryL10n.load()


    def loadVehicleDictionary(self):
        for (idx, nation_path) in TankListPaths.INDEX.items():
            list_xml_path = os.path.join(nation_path, 'list.xml')
            listNodes = self.res_mgr.open_scripts_xml(list_xml_path)

            if listNodes is None:
                continue

            for node in listNodes:
                try:
                    vehicle_userString = node.find('userString').text.strip()
                except:continue

                vehicle_props = 0

                vehicle_tags = node.find('tags').text.strip().split()
                if 'secret' in vehicle_tags:
                    vehicle_props |= 1

                    if vehicle_userString.startswith('#igr_vehicles:'):
                        continue

                    if vehicle_userString.endswith(('_bot', '_fallout')):
                        continue

                    if node.tag.endswith(('_training', '_fallout')):
                        continue

                vehicle_category = vehicle_tags[0]
                if vehicle_category not in g_CategoryL10n.ALL:
                    continue

                if 'NY2019_style' in vehicle_tags:
                    vehicle_props |= 2

                vehicle_category = g_CategoryL10n.ALL[vehicle_category]
                vehicle_level = tv_AsInt(node.find('level').text)
                vehicle_xml = '%s.xml' % node.tag
                vehicle_l10n_name = g_MoFiles._(vehicle_userString)

                self.sqlite3_cursor.execute('INSERT INTO TankList_table(tank_nation, tank_type_id, tank_props, tank_name, tank_name_in_res, tank_level) VALUES (?, ?, ?, ?, ?, ?)', (
                    idx, vehicle_category, vehicle_props,
                    vehicle_l10n_name, vehicle_xml, vehicle_level))


    def loadInfo(self, xml_file, nation_idx):
        xml_path = os.path.join(TankListPaths.INDEX[nation_idx], xml_file)

        element = self.res_mgr.open_scripts_xml(xml_path)

        return FullTankDesc(element)
