''' SkepticalFox 2015-2021 '''

bl_info = {
    "name": "World of Tanks - Vehicle Viewer",
    "author": "SkepticalFox",
    "version": (0, 1, 51),
    "blender": (2, 93, 0),
    "location": "View3D > Tool Shelf > TankViewer",
    "description": "WoT Tank Viewer",
    "warning": "Test version",
    "wiki_url": "https://koreanrandom.com/forum/topic/28240-/",
    "repo_url": "https://bitbucket.org/SkepticalFox/bigworld-blender-tools-wot-wowp-wows/",
    "category": "3D View",
}


import os
import builtins

from .settings import *
from .common import *
from .BigWorldModelLoader import *
from .VehicleUtils import *

import bpy
import bpy.utils.previews


# TODO: @dataclass(init=False)
class PluginData:

    wot_data_manager = None
    current_tank_info = None
    current_tank_name = ''
    current_tank_xml_file = ''

    def __init__(self):
        raise Exception('This is static class!')

    @classmethod
    def clear(cls):
        cls.current_tank_name = ''
        cls.current_tank_xml_file = ''
        if cls.wot_data_manager is not None:
            del cls.wot_data_manager

    @classmethod
    def load_client_data(cls):
        addon_prefs = user_preferences.addons[__name__].preferences
        wot_dir_path = addon_prefs.world_of_tanks_game_path
        cls.wot_data_manager = WotDataManager.add_client(wot_dir_path)
        return cls.wot_data_manager is not None

    @classmethod
    def load_tank_info(cls, nation_idx):
        cls.current_tank_info = cls.wot_data_manager.loadInfo(cls.current_tank_xml_file, nation_idx)

    wot_version = None
    current_tank_name = ''
    current_tank_xml_file = ''
    turret_l10n_items = []
    gun_l10n_items = {}
    custom_icons = None
    current_veh_models = {}



builtins.PluginDataG = PluginData
user_preferences = bpy.context.preferences



#####################################################################
# Load models

def loadModels(col, model_list, kill_dict,
               use_custom_dir = False):
    p = user_preferences.addons[__package__].preferences
    use_global_undo_orig = user_preferences.edit.use_global_undo
    user_preferences.edit.use_global_undo = False
    try:
        new_primitives_format = PluginData.wot_data_manager.wot_version >= (0, 9, 12)
    except:
        new_primitives_format = False

    for model in model_list:
        g_BigWorldModelLoader.load(
            col, model, kill_dict,
            p.world_of_tanks_custom_path,
            use_custom_dir, new_primitives_format)
    user_preferences.edit.use_global_undo = use_global_undo_orig



#####################################################################
# Delete models

def deleteModels(kill_dict):
    use_global_undo_orig = user_preferences.edit.use_global_undo
    user_preferences.edit.use_global_undo = False

    if kill_dict.get('Objects'):
        for object_name in kill_dict['Objects']:
            if bpy.data.objects.get(object_name):
                object_ = bpy.data.objects[object_name]
                #bpy.context.scene.objects.unlink(object_)
                bpy.data.objects.remove(object_)

    if kill_dict.get('Meshes'):
        for mesh_name in kill_dict['Meshes']:
            if bpy.data.meshes.get(mesh_name):
                mesh_ = bpy.data.meshes[mesh_name]
                bpy.data.meshes.remove(mesh_)

    if kill_dict.get('Materials'):
        for material_name in kill_dict['Materials']:
            if bpy.data.materials.get(material_name):
                material_ = bpy.data.materials[material_name]
                bpy.data.materials.remove(material_, do_unlink=True)

    if kill_dict.get('Textures'):
        for texture_name in kill_dict['Textures']:
            if bpy.data.textures.get(texture_name):
                texture_ = bpy.data.textures[texture_name]
                bpy.data.textures.remove(texture_)

    if kill_dict.get('Images'):
        for image_name in kill_dict['Images']:
            if bpy.data.images.get(image_name):
                image_ = bpy.data.images[image_name]
                bpy.data.images.remove(image_)

    if kill_dict.get('Curves'):
        for curve_name in kill_dict['Curves']:
            if bpy.data.curves.get(curve_name):
                curve_ = bpy.data.curves[curve_name]
                bpy.data.curves.remove(curve_)

    kill_dict.clear()
    kill_dict['Meshes'] = []
    kill_dict['Curves'] = []
    kill_dict['Objects'] = []
    kill_dict['Images'] = []
    kill_dict['Materials'] = []
    kill_dict['Textures'] = []

    user_preferences.edit.use_global_undo = use_global_undo_orig



#####################################################################
# Apply Path Operator

class Apply_Path_Operator(bpy.types.Operator):
    bl_label = 'Operator apply paths'
    bl_idname = 'tv.apply_path_op'

    def execute(self, context):
        if PluginData.load_client_data():
            nation_l10n_items = []
            for idx, nation_l10n in g_NationsL10n.INDEX.items():
                nation_l10n_items.append((str(idx), nation_l10n, ''))
            nation_l10n_items.sort()

            category_l10n_items = []
            for idx, category_l10n in g_CategoryL10n.INDEX.items():
                category_l10n_items.insert(idx, (str(idx), category_l10n, ''))

            level_l10n_items = []
            for idx, level_l10n in g_LevelsL10n.INDEX.items():
                level_l10n_items.insert(idx, (str(idx), level_l10n, ''))

            # register
            bpy.utils.register_class(Apply_Filter_Operator)
            bpy.utils.register_class(Apply_Vehicle_Operator)
            bpy.utils.register_class(UI_UL_Tank_List)
            bpy.utils.register_class(CustomProp)
            bpy.types.Scene.custom = bpy.props.CollectionProperty(type = CustomProp)
            bpy.types.Scene.custom_index = bpy.props.IntProperty()

            bpy.types.Scene.nations_l10n = bpy.props.EnumProperty(
                name = 'Nations',
                description = '',
                items = nation_l10n_items)

            bpy.types.Scene.category_l10n = bpy.props.EnumProperty(
                name = 'Category',
                description = '',
                items = category_l10n_items)

            bpy.types.Scene.levels_l10n = bpy.props.EnumProperty(
                name = 'Tier',
                description = '',
                items = level_l10n_items)

            bpy.utils.register_class(PANEL_PT_TankFilter)
            bpy.utils.register_class(PANEL_PT_TankList)

            # unregister
            bpy.utils.unregister_class(PANEL_PT_TankViewer_Start)
            bpy.utils.unregister_class(Apply_Path_Operator)

        else:
            # INFO, WARNING or ERROR
            self.report({'WARNING'}, 'Error in path!')

        return {'FINISHED'}



#####################################################################
# Apply Filter Operator

class Apply_Filter_Operator(bpy.types.Operator):
    bl_label = 'Operator apply filter'
    bl_idname = 'tv.apply_filter'

    def execute(self, context):
        scn = context.scene
        custom = scn.custom
        scn.custom_index = 0
        custom.clear()

        if PluginData.wot_data_manager is not None:
            level = int(scn.levels_l10n)
            category = int(scn.category_l10n)
            nation = int(scn.nations_l10n)

            str_exec = 'SELECT tank_nation, tank_props, tank_name, tank_name_in_res FROM TankList_table'
            str_q = []
            if nation:
                str_q.append('tank_nation=%s' % nation)
            if category:
                str_q.append('tank_type_id=%s' % category)
            if level:
                str_q.append('tank_level=%s' % level)
            if str_q:
                str_exec += ' WHERE ' + ' AND '.join(str_q) + ';'
            for row in PluginData.wot_data_manager.sqlite3_cursor.execute(str_exec):
                tank_list_item = custom.add()
                tank_list_item.nation_idx = row[0]
                tank_list_item.icon_locked = (row[1] & 1)
                tank_list_item.NYst = (row[1] >> 1) & 1
                tank_list_item.name = row[2]
                tank_list_item.xml_file = row[3]

        return {'FINISHED'}



#####################################################################
# loadByVehicleInfo

def loadByVehicleInfo(scene):
    deleteModels(PluginData.current_veh_models)

    from mathutils import Vector

    model_list = []

    chassis = PluginData.current_tank_info.chassis
    hull = PluginData.current_tank_info.hull
    turret = PluginData.current_tank_info.turrets0[scene.turrets_l10n]

    model_style = scene.tank_style
    if not model_style:
        model_style = 'default'

    model_status = 'undamaged_model'
    if scene.tank_status in (
            'undamaged_model',
            'destroyed_model',
            'collision_model'):
        model_status = scene.tank_status

    # TODO: rework
    def get_path_by_style_status(d):
        if model_status == 'collision_model':
            return d.collision_model
        if model_style in d.sets:
            return getattr(d.sets[model_style], model_status)
        return getattr(d.sets['default'], model_status)

    if not scene.use_segment_tracks or model_status != 'undamaged_model':
        model_list.append({
            'File' : get_path_by_style_status(chassis),
            'Scale' : Vector((1.0, 1.0, 1.0)),
            'Rotation' : Vector((0.0, 0.0, 0.0)),
            'Position' : Vector((0.0, 0.0, 0.0))
        })

    else:
        model_list.append({
            'File' : get_path_by_style_status(chassis),
            'Scale' : Vector((1.0, 1.0, 1.0)),
            'Rotation' : Vector((0.0, 0.0, 0.0)),
            'Position' : Vector((0.0, 0.0, 0.0)),
            'use_segment' : True
        })

        splineDesc = chassis.splineDesc
        if splineDesc:
            segments = splineDesc.sets.get(model_style) or splineDesc.sets['default']

            model_list.append({
                'File' : segments.segmentModelLeft,
                'Scale' : Vector((1.0, 1.0, 1.0)),
                'Rotation' : Vector((0.0, 0.0, 0.0)),
                'Position' : Vector((0.0, splineDesc.segmentOffset, 0.0)),
                'is_segment' : True,
                'track_file' : splineDesc.left,
                'segmentOffset' : splineDesc.segmentLength
            })

            model_list.append({
                'File' : segments.segmentModelRight,
                'Scale' : Vector((1.0, 1.0, 1.0)),
                'Rotation' : Vector((0.0, 0.0, 0.0)),
                'Position' : Vector((0.0, splineDesc.segmentOffset, 0.0)),
                'is_segment' : True,
                'track_file' : splineDesc.right,
                'segmentOffset' : splineDesc.segmentLength
            })

            if segments.has_two_segments():
                model_list.append({
                    'File' : segments.segment2ModelLeft,
                    'Scale' : Vector((1.0, 1.0, 1.0)),
                    'Rotation' : Vector((0.0, 0.0, 0.0)),
                    'Position' : Vector((0.0, splineDesc.segmentLength/2, 0.0)),
                    'is_segment' : True,
                    'track_file' : splineDesc.left,
                    'segmentOffset' : splineDesc.segmentLength
                })

                model_list.append({
                    'File' : segments.segment2ModelRight,
                    'Scale' : Vector((1.0, 1.0, 1.0)),
                    'Rotation' : Vector((0.0, 0.0, 0.0)),
                    'Position' : Vector((0.0, splineDesc.segmentLength/2, 0.0)),
                    'is_segment' : True,
                    'track_file' : splineDesc.right,
                    'segmentOffset' : splineDesc.segmentLength
                })

    model_list.append({
        'File' : get_path_by_style_status(turret),
        'Scale' : Vector((1.0, 1.0, 1.0)),
        'Rotation' : Vector((0.0, 0.0, 0.0)),
        'Position' : hull.turretPositions
    })

    model_list.append({
        'File' : get_path_by_style_status(hull),
        'Scale' : Vector((1.0, 1.0, 1.0)),
        'Rotation' : Vector((0.0, 0.0, 0.0)),
        'Position' : chassis.hullPosition
    })

    model_list.append({
        'File' : get_path_by_style_status(turret.guns[scene.guns_l10n]),
        'Scale' : Vector((1.0, 1.0, 1.0)),
        'Rotation' : Vector((0.0, 0.0, 0.0)),
        'Position' : turret.gunPosition
    })

    use_custom_dir = False
    if scene.texture_location == 'custom':
        use_custom_dir = True

    if 'Tank Collection' not in bpy.data.collections:
        col = bpy.data.collections.new('Tank Collection')
        scene.collection.children.link(col)
    else:
        col = bpy.data.collections['Tank Collection']

    loadModels(col, model_list, PluginData.current_veh_models, use_custom_dir)



#####################################################################
# Apply Vehicle Operator

class Apply_Vehicle_Operator(bpy.types.Operator):
    bl_label = 'Operator apply vehicle'
    bl_idname = 'tv.apply_vehicle'

    @classmethod
    def poll(self, context):
        if len(context.scene.custom):
            custom = context.scene.custom[context.scene.custom_index]
            if (PluginData.current_tank_name != custom.name) and (PluginData.current_tank_xml_file != custom.xml_file) and ((context.active_object is None) or (context.active_object.mode == 'OBJECT')):
                return True
        return False

    def execute(self, context):
        if PluginData.current_tank_name == '':
            registerModelSettings()

        scn = context.scene
        custom = scn.custom[scn.custom_index]
        PluginData.current_tank_name = custom.name
        PluginData.current_tank_xml_file = custom.xml_file

        scn.use_segment_tracks = False
        scn.tank_status = 'undamaged_model'
        scn.NYst = custom.NYst
        scn.tank_style = 'default'

        PluginData.load_tank_info(custom.nation_idx)
        PluginData.turret_l10n_items = t_items = []
        PluginData.gun_l10n_items.clear()
        for turret_name, turret_info in PluginData.current_tank_info.turrets0.items():
            t_items.append((turret_name, turret_name, ''))
            gun_list = []
            for gun_name, gun_info in turret_info.guns.items():
                gun_list.append((gun_name, gun_name, ''))
            PluginData.gun_l10n_items[turret_name] = gun_list

        try:
            t_items.sort()
            scn.turrets_l10n = t_items[0][0]
            PluginData.gun_l10n_items[t_items[0][0]].sort()
            scn.guns_l10n = PluginData.gun_l10n_items[t_items[0][0]][0][0]
        except:pass

        tv_PrintSplitter()
        tv_PrintInfo('tank name: `%s`' % PluginData.current_tank_name)
        try:
            loadByVehicleInfo(scn)
        except:
            self.report({'ERROR'}, 'Error in load `%s`!' % PluginData.current_tank_name)
            import traceback
            traceback.print_exc()

        return {'FINISHED'}



#####################################################################
# Reopen vehicle operator

class Reopen_Vehicle_Operator(bpy.types.Operator):
    bl_label = 'Operator reopen vehicle'
    bl_idname = 'tv.reopen_vehicle'

    @classmethod
    def poll(self, context):
        if (context.active_object is None) or (context.active_object.mode == 'OBJECT'):
            return True
        return False

    def execute(self, context):
        tv_PrintSplitter()
        tv_PrintInfo('reopen tank')
        try:
            loadByVehicleInfo(context.scene)
        except:
            self.report({'ERROR'}, 'Error in reopen `%s`!' % PluginData.current_tank_name)
            import traceback
            traceback.print_exc()

        return {'FINISHED'}



#####################################################################
# turret_l10n_items_callback

def turret_l10n_items_callback(scene, context):
    if context is not None:
        return PluginData.turret_l10n_items
    return []



#####################################################################
# gun_l10n_items_callback

def gun_l10n_items_callback(scene, context):
    if context is not None:
        return PluginData.gun_l10n_items.get(context.scene.turrets_l10n)
    return []



#####################################################################
# tank_style_items_callback

def tank_style_items_callback(scene, context):
    res = [('default', 'default', '')]
    if context is not None and context.scene.NYst:
        res.append(('NYst', 'NYst', ''))
    return res



#####################################################################
# register ModelSettings

def registerModelSettings():
    bpy.utils.register_class(PANEL_PT_TankViewer_ModelSettings)
    bpy.utils.register_class(Reopen_Vehicle_Operator)

    bpy.types.Scene.turrets_l10n = bpy.props.EnumProperty(
        name = 'Turret',
        description = '',
        items = turret_l10n_items_callback
    )

    bpy.types.Scene.guns_l10n = bpy.props.EnumProperty(
        name = 'Gun',
        description = '',
        items = gun_l10n_items_callback
    )

    bpy.types.Scene.use_segment_tracks = bpy.props.BoolProperty(
        name = 'Use segment tracks (Experimental)',
        description = '',
        default = False
    )

    bpy.types.Scene.NYst = bpy.props.BoolProperty(
        name = 'NYst',
        description = 'NYst style',
        default = False
    )

    bpy.types.Scene.tank_style = bpy.props.EnumProperty(
        name = 'Tank style',
        description = '',
        items = tank_style_items_callback
    )

    bpy.types.Scene.tank_status = bpy.props.EnumProperty(
        name = 'Tank Mesh',
        description = '',
        items = (
            ('undamaged_model', 'Undamaged model', ''),
            ('destroyed_model', 'Destroyed model', ''),
            ('collision_model', 'Collision model', '')
        )
    )

    bpy.types.Scene.texture_location = bpy.props.EnumProperty(
        name = 'Texture Location',
        description = '',
        items = (
            ('custom', 'Mod folder (*.png; *.dds)', ''),
            ('packages', 'Pkg Files', '')
        )
    )



#####################################################################
# unregister ModelSettings

def unregisterModelSettings():
    bpy.utils.unregister_class(PANEL_PT_TankViewer_ModelSettings)

    del bpy.types.Scene.use_segment_tracks

    del bpy.types.Scene.tank_status
    del bpy.types.Scene.texture_location



#####################################################################
# Panel TankViewer Settings

class PANEL_PT_TankViewer_ModelSettings(bpy.types.Panel):
    bl_label = 'Tank Options'
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS'
    bl_category = ''
    bl_options = {'DEFAULT_CLOSED'}

    def draw(self, context):
        layout = self.layout
        scn = context.scene

        if scn.NYst:
            layout.prop(scn, 'tank_style')
            layout.separator()

        box = layout.row().box()
        box.prop(scn, 'turrets_l10n')
        box.prop(scn, 'guns_l10n')

        layout.separator()

        layout.prop(scn, 'use_segment_tracks')

        layout.separator()

        box = layout.row().box()
        box.prop(scn, 'tank_status')

        layout.separator()

        box = layout.row().box()
        box.prop(scn, 'texture_location')

        layout.separator()

        layout.operator('tv.reopen_vehicle', text='Reload model')



#####################################################################
# Panel TankViewer Start

class PANEL_PT_TankViewer_Start(bpy.types.Panel):
    bl_label = 'Start'
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS'
    bl_category = ''

    def draw(self, context):
        layout = self.layout

        layout.operator('tv.apply_path_op', text='Parse WoT')



#####################################################################
# Tank Filter Panel

class PANEL_PT_TankFilter(bpy.types.Panel):
    bl_label = 'Tank Filter'
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS'
    bl_category = ''

    def draw(self, context):
        layout = self.layout

        scn = context.scene

        box = layout.row().box()
        box.prop(scn, 'nations_l10n', text='')
        box.prop(scn, 'category_l10n', text='')
        box.prop(scn, 'levels_l10n', text='')

        layout.separator()
        layout.operator('tv.apply_filter', text='Apply Filter')



#####################################################################
# CustomProp

class CustomProp(bpy.types.PropertyGroup):
    name : bpy.props.StringProperty()
    xml_file : bpy.props.StringProperty()
    nation_idx : bpy.props.IntProperty()
    icon_locked : bpy.props.BoolProperty()
    NYst : bpy.props.BoolProperty()



#####################################################################
# UI_Tank_List

class UI_UL_Tank_List(bpy.types.UIList): 
    def draw_item(self, context, layout, data, item, icon, active_data, active_propname, index):
        if not item.icon_locked:
            layout.label(text=item.name, translate=False)

        else:
            layout.label(text=item.name, icon='LOCKED', translate=False)



#####################################################################
# Panel Tank List

class PANEL_PT_TankList(bpy.types.Panel):
    bl_label = 'Tank List'
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS'
    bl_category = ''

    def draw(self, context):
        layout = self.layout
        scn = context.scene
        layout.row().template_list('UI_UL_Tank_List', '', scn, 'custom', scn, 'custom_index')

        layout.separator()
        layout.operator('tv.apply_vehicle', text='Load Tank')



#####################################################################
# unregister

def unregister():
    try: bpy.types.TOPBAR_MT_help.remove(menu_func_tankviewer) # blender 2.8
    except: bpy.types.INFO_MT_help.remove(menu_func_tankviewer)
    bpy.utils.unregister_class(INFO_MT_tankviewer)
    bpy.utils.unregister_class(TankViewerPreferences)

    bpy.app.translations.unregister(__package__)


    if PluginData.wot_data_manager is not None:
        if PluginData.current_tank_name:
            unregisterModelSettings()
        PluginData.clear()
        bpy.context.scene.custom.clear()
        del bpy.types.Scene.custom
        del bpy.types.Scene.custom_index

        bpy.utils.unregister_class(UI_UL_Tank_List)
        bpy.utils.unregister_class(PANEL_PT_TankFilter)
        bpy.utils.unregister_class(PANEL_PT_TankList)
        bpy.utils.unregister_class(CustomProp)
        bpy.utils.unregister_class(Apply_Filter_Operator)
        bpy.utils.unregister_class(Apply_Vehicle_Operator)
        bpy.utils.unregister_class(Reopen_Vehicle_Operator)

    else:
        bpy.utils.unregister_class(PANEL_PT_TankViewer_Start)
        bpy.utils.unregister_class(Apply_Path_Operator)



#####################################################################
# TankViewerPreferences

class TankViewerPreferences(bpy.types.AddonPreferences):
    bl_idname = __package__

    world_of_tanks_game_path : bpy.props.StringProperty(
        name = 'WoT Path',
        subtype = 'DIR_PATH',
        default = WOT_PATH_DEFAULT
    )

    world_of_tanks_custom_path : bpy.props.StringProperty(
        name = 'Custom Path',
        subtype = 'DIR_PATH',
        default = WOT_CUSTOM_PATH_DEFAULT
    )

    def draw(self, context):
        layout = self.layout

        layout.prop(self, 'world_of_tanks_game_path')
        layout.prop(self, 'world_of_tanks_custom_path')



#####################################################################
# register

def register():
    bpy.utils.register_class(TankViewerPreferences)

    if not PluginData.custom_icons:
        PluginData.custom_icons = bpy.utils.previews.new()
        icons_directory = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'icons')
        PluginData.custom_icons.load('koreanrandom', os.path.join(icons_directory, 'koreanrandom.png'), 'IMAGE')

    bpy.utils.register_class(PANEL_PT_TankViewer_Start)
    bpy.utils.register_class(Apply_Path_Operator)
    bpy.utils.register_class(INFO_MT_tankviewer)

    try: bpy.types.TOPBAR_MT_help.prepend(menu_func_tankviewer) # blender 2.8
    except: bpy.types.INFO_MT_help.prepend(menu_func_tankviewer)

    bpy.app.translations.register(__package__, translations_dict)



#####################################################################
# menu_func

def menu_func_tankviewer(self, context):
    self.layout.menu('INFO_MT_tankviewer')



#####################################################################
# INFO_MT_tankviewer

class INFO_MT_tankviewer(bpy.types.Menu):
    bl_label = 'About TankViewer'

    def draw(self, context):
        layout = self.layout

        layout.label(text='Version: %d.%d.%d' % bl_info['version'][:])
        layout.label(text='Author: %s' % bl_info['author'])

        layout.separator()

        layout.operator('wm.url_open', text='TankViewer Website', icon_value=PluginData.custom_icons['koreanrandom'].icon_id).url = bl_info['wiki_url']
        layout.operator('wm.url_open', text='TankViewer Repository', icon='URL').url = bl_info['repo_url']
