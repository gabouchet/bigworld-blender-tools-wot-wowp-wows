''' SkepticalFox 2015-2018 '''



#####################################################################
# imports

from mathutils import Vector, Matrix
from ..common.XmlUnpacker import XmlUnpacker



#####################################################################
# globals

g_XmlUnpacker = XmlUnpacker()



#####################################################################
# functions

def StrToVector(vector_str):
    if vector_str:
        return Vector(tuple(map(float, vector_str.strip().split())))
tv_AsVector = StrToVector



def tv_AsMatrix4x4T(vector_str):
    vector_16 = tv_AsVector(vector_str)
    return Matrix(
        (vector_16[:4], vector_16[4:8], vector_16[8:12], vector_16[12:16])).transposed()



def tv_AsBool(bool_str):
    if 'true' in bool_str.lower():
        return True
    return False



def tv_AsInt(int_str):
    if int_str is None:
        tv_PrintError('int_str is None')
    int_str = int_str.strip()
    if int_str.isdigit():
        return int(int_str)
    return 0



def tv_AsFloat(float_str):
    return float(float_str)



def StrToFilePath(path_str):
    if path_str is not None:
        return '/'.join(path_str.strip().split('\\'))
tv_AsNormPath = StrToFilePath



def tv_Log(log_str):
    if True:
        print(log_str)
    else:
        with open('log.txt', 'a') as f:
            f.write(log_str)



def PrintInfo(info_str):
    tv_Log('Info: %s' % info_str)
tv_PrintInfo = PrintInfo



def tv_PrintWarn(warn_str):
    tv_Log('Warn: %s' % warn_str)



def tv_PrintError(err_str):
    tv_Log('Error: %s' % err_str)



def PrintSplitter():
    tv_Log('='*12)
tv_PrintSplitter = PrintSplitter



def tv_UnpackNormal(packed):
    pky = (packed>>22)&0x1FF
    pkz = (packed>>11)&0x3FF
    pkx = packed&0x3FF
    x = pkx/1023.0
    if pkx & (1<<10):
        x = -x
    y = pky/511.0
    if pky & (1<<9):
        y = -y
    z = pkz/1023.0
    if pkz & (1<<10):
        z = -z
    return Vector((x, z, y))



def tv_UnpackNormal_tag3(packed):
    pkz = (packed>>16)&0xFF^0xFF
    pky = (packed>>8)&0xFF^0xFF
    pkx = packed&0xFF^0xFF
    if pkx > 0x7f:
        x = -float(pkx&0x7f)/0x7f
    else:
        x = float(pkx^0x7f)/0x7f
    if pky > 0x7f:
        y = -float(pky&0x7f)/0x7f
    else:
        y = float(pky^0x7f)/0x7f
    if pkz>0x7f:
        z = -float(pkz&0x7f)/0x7f
    else:
        z = float(pkz^0x7f)/0x7f
    return Vector((x, z, y))
