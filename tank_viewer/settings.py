''' SkepticalFox 2015-2018 '''



#####################################################################
# Default path settings

WOT_PATH_DEFAULT = 'C:\\Games\\World_of_Tanks'
WOT_CUSTOM_PATH_DEFAULT = 'C:\\Games\\World_of_Tanks\\res_mods\\1.3.0.1'



#####################################################################
# Translations dict

translations_dict = {
    'ru_RU' : {
        ('Operator', 'Parse WoT') : 'Пуск',
        ('Operator', 'Load Tank') : 'Показать',
        ('Operator', 'Basic Hangar') : 'Обычный',
        ('Operator', 'Premium Hangar') : 'Премиум',
        ('Operator', 'Apply Filter') : 'Применить фильтр',
        ('Operator', 'Reload model') : 'Применить',
        ('*', 'WoT Path') : 'Путь к WoT',
        ('*', 'Custom Path') : 'Пользовательская папка',
        ('*', 'Hangars') : 'Ангары',
        ('*', 'Tank List') : 'Список техники',
        ('*', 'Tank Filter') : 'Фильтры',
        ('*', 'Tank Options') : 'Настройки техники',
        ('*', 'Hide Geometry') : 'Скрыть элементы:',
        ('*', 'Turret') : 'Башня',
        ('*', 'Gun') : 'Орудие',
        ('*', 'Hull') : 'Корпус',
        ('*', 'Chassis') : 'Ходовая',
        ('*', 'Use segment tracks (Experimental)') : 'Сегментные траки (Экспериментально)',
        ('*', 'Texture Location') : 'Расположение текстур',
        ('*', 'Tank Mesh') : 'Состояние танка',
        ('*', 'Undamaged model') : 'Целый',
        ('*', 'Destroyed model') : 'Уничтожен',
        ('*', 'Collision model') : 'Бронирование',
        ('*', 'Tier') : 'Уровень',
        ('*', 'Category') : 'Класс',
        ('*', 'Nations') : 'Нация',
    }
}
